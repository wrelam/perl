#!/usr/bin/perl

# Author:	Walt Elam
# Date:		11/11/11
#
# Description:
# This program will take two numbers from user input and then display their product.
#

print "Product Calculator\n";
print "******************\n\n";

print "Please enter two numbers.\n";

# Global Variables
$num1 = 0;
$num2 = 0;

# Get first number
print "Number 1: ";

$line = <STDIN>;

if ($line eq "\n")
{
	print "Incorrect input.\n";
	exit(0);
}
else
{
	$num1 = $line;
}

# Get second number
print "Number 2: ";

$line = <STDIN>;

if ($line eq "\n")
{
	print "Incorrect input.\n";
	exit(0);
}
else
{
	$num2 = $line;
}

# Output result
print "The product is: ".$num1*$num2."\n";

