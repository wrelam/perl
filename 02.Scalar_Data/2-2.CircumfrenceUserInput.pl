#!/usr/bin/perl

# Author:	Walt Elam
# Date:		11/11/11
#
# Description:
# This program will calculate the circumfrence of a circle where the radius is
# defined by user input
#

# import package to get Pi
use Math::Trig;

print "Circumfrence Calculator 2\n";
print "*************************\n\n";

# Prompt for radius
print "Please enter a radius: ";

# Assign input to radius
$rad = 0;
$line = <STDIN>;

if ($line eq "\n")
{
	print "Not a valid number\n";
}
else
{
	$rad = $line;
	print "You entered: ".$rad;
}

# calculate circumfrence
$circ = $rad * 2 * pi;

print "The circumfrence is ".$circ."\n";
