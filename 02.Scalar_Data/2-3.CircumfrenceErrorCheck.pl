#!/usr/bin/perl

# Author:	Walt Elam
# Date:		11/11/11
#
# Description:
# This program will calculate the circumfrence of a circle where the radius is
# defined by user input and checks their input for negative values.
#

# import package to get Pi
use Math::Trig;

print "Circumfrence Calculator 3\n";
print "*************************\n\n";

# Prompt for radius
print "Please enter a radius: ";

# Assign input to radius
$rad = 0;
$line = <STDIN>;

if ($line eq "\n")
{
	print "Not a valid number\n";
}
elsif ($line < 0)
{
	$rad = 0;
}
else
{
	$rad = $line;
}

# calculate circumfrence
$circ = $rad * 2 * pi;

print "The circumfrence is ".$circ."\n";
