#!/usr/bin/perl

# Author:	Walt Elam
# Date:		11/11/11
#
# Description:
# This program will calculate the circumfrence of a circle with radius of
# 12.5, or whatever is defined
#

# import package to get Pi
use Math::Trig;

print "Circumfrence Calculator\n";
print "***********************\n\n";

# Define radius
$rad = 12.5;

# calculate circumfrence
$circ = $rad * 2 * pi;

print "The circumfrence is ".$circ."\n";
