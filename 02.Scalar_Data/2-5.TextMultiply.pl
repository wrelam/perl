#!/usr/bin/perl

# Author:	Walt Elam
# Date:		11/11/11
#
# Description:
# This program will take two numbers from user input and then display their product.
#

print "Text Multiplier\n";
print "***************\n\n";

print "Please enter a word and a number.\n";

# Global Variables
$text = "";
$num = 0;

# Get text input
print "Enter a word: ";

$line = <STDIN>;

if ($line eq "\n")
{
	print "You didn't enter anything!\n";
	exit(0);
}
else
{
	$text = $line;
}

# Get number input
print "Enter a number: ";

$line = <STDIN>;

if ($line eq "\n")
{
	print "You didn't enter anything!\n";
	exit(0);
}
else
{
	$num = $line;
}

# Output their word the number of times they entered
print $text x $num;
