#!/usr/bin/perl
use strict;
use warnings;

# Greet the current person by indicating everyone present

sub greet
{
    state @people;
    print "Welcome @_!";

    if (!@people)
    {
        print " You are the first one here!\n";
    }
    else
    {
        print " I've seen @people.\n";
    }

    push(@people, shift);
}

greet("Fred");
greet("Barney");
greet("Wilma");
greet("Betty");


