#!/usr/bin/perl
use strict;
use warnings;

# Write a subroutine which returns the sum of a list of numbers

sub total
{
    my $sum = 0;
    foreach (@_)
    {
        $sum = $sum + $_;
    }
    return $sum;
}

my @fred = qw( 1 3 5 7 9 );
my $tot = &total(@fred);
print "The total of @fred is $tot\n";

