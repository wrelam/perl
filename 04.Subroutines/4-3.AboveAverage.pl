#!/usr/bin/perl
use strict;
use warnings;

# Sum the numbers 1 to 1000

sub above_average
{
    my $sum = 0;
    my $count = 0;
    my $avg = 0;
    my @above;

    foreach (@_)
    {
        $sum = $sum + $_;
        $count++;
    }

    $avg = $sum / $count;

    foreach (@_)
    {
        if ($_ > $avg)
        {
            push(@above, $_);
        }
    }

    return @above;
}

my @fred = &above_average(1..10);
print "Above: @fred\n";
@fred = &above_average(100, 1..10);
print "Above: @fred\n";


