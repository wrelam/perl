#!/usr/bin/perl
use strict;
use warnings;

# Sum the numbers 1 to 1000

sub total
{
    my $sum = 0;
    foreach (@_)
    {
        $sum = $sum + $_;
    }
    return $sum;
}

my $tot = &total(1..1000);
print "The total of (1..1000) is $tot\n";

