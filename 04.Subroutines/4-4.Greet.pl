#!/usr/bin/perl
use strict;
use warnings;
use 5.010;

# Greet the current person by indicating the last person to enter

sub greet
{
    state $last_person = "";
    print "Welcome @_!";

    if ($last_person eq "")
    {
        print " You are the first one here!\n";
    }
    else
    {
        print " $last_person is also here!\n";
    }
    $last_person = shift;
}

&greet("Fred");
&greet("Barney");


