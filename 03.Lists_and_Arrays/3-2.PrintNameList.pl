#!/usr/bin/perl

# Reads numbers on seperate lines from STDIN and prints out the corresponding
# name from a list of predetermined names
#

@names = qw(fred betty barney dino wilma pebbles bamm-bamm);
$numNames = $#names + 1;

@numbers = <STDIN>;

foreach $num (@numbers)
{
    $i = $num - 1;

    if ( $i < $numNames )
    {
        print $names[$i] . "\n";
    }
}

