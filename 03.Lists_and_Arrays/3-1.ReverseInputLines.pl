#!/usr/bin/perl

# Reads a list of strings until end-of-input and prints the list in reverse
# order. Push Ctrl+D in Unix, or Ctrl-Z on DOS to signal end-of-input on command
# line.
#

# Get input file
@lines = <STDIN>;

@lines = reverse @lines;

print @lines;
