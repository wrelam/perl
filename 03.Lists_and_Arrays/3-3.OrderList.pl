#!/usr/bin/perl

# Reads a list of strings on separate lines and prints them out in code point
# order.
#

@names = <STDIN>;

@names = sort @names;

print @names;

chomp(@names);

foreach $name (@names)
{
    print $name . " ";
}

print "\n";
